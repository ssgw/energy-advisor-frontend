import type { Config } from 'tailwindcss'

export default {
  content: ['./index.html', './src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    fontFamily: {
      sans: 'univers',
    },
    extend: {
      colors: {
        // primary
        red: '#E00025',
        black: '#000000',
        white: '#ffffff',
        // secondary
        darkred: '#BD0221',
        lightred: '#FFF5F3',
        green: '#007000',
        lightgreen: '#EEFFEE',
        darkgray: '#505050',
        lightgray: '#EFEFEB',
        olivegray: '#ADAA9F',
        beige: '#D8D5C5',
        lightbeige: '#E3E2DA',
        // tertiary
        yellow: '#F9DA19',
        buttercup: '#F49C24',
        cornflower: '#84C8E8',
        silvertree: '#59B892',
        bluebell: '#9A98CB',
        sandrift: '#A98471',
      },
    },
  },
  plugins: [],
} as Config
