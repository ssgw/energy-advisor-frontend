import { Router, RouterProvider } from '@tanstack/react-router'
import { indexRoute } from './routes'
import { rootRoute } from './routes/root'
import { componentsRoute } from './routes/components-test'
import { analysisRoute } from './routes/analysis'
import { componenTest2Route } from './routes/components-test2'

const routeTree = rootRoute.addChildren([
  indexRoute,
  componentsRoute,
  analysisRoute,
  componenTest2Route,
])

const router = new Router({
  defaultPreload: 'intent',
  routeTree,
})

declare module '@tanstack/react-router' {
  interface Register {
    router: typeof router
  }
}

export function App() {
  return <RouterProvider router={router} />
}
