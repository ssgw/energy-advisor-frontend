import { PlacesContext } from '@/lib/stores/places.store'
import { Outlet, RootRoute, RouterContext } from '@tanstack/react-router'

// const routerContext = new RouterContext<PlacesContext>()

export const rootRoute = new RootRoute({
  component: () => (
    <div>
      <Outlet />
    </div>
  ),
})
