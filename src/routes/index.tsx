import { AddressAutocomplete } from '@/components/address-autocomplete'
import { Button } from '@/components/ui/button'
import { Place } from '@/lib/models/place'
import { Marker } from '@react-google-maps/api'
import { Link, Route } from '@tanstack/react-router'
import { useState } from 'react'
import { rootRoute } from './root'
import { Header } from '@/components/header'

export const indexRoute = new Route({
  getParentRoute: () => rootRoute,
  path: '/',
  component: ({ useContext }) => {
    const [selectedPlace, setSelectedPlace] = useState<Place | undefined>(
      undefined
    )

    const [marker, setMarker] = useState<Marker | undefined>(undefined)

    function selectPlaceHandler(place: Place) {
      setSelectedPlace(place)

      setMarker(
        new Marker({
          position: {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng(),
          },
        })
      )
    }

    function getSearchParams(place: Place) {
      return place
    }

    return (
      <div className="flex flex-col gap-2 justify-center">
        <Header setPlaceDetails={(place) => setSelectedPlace(place)} />
        <div className="mx-auto max-w-4xl px-3 grid gap-3">
          <div className="min-w-0">{/* <Map marker={marker} /> */}</div>

          {selectedPlace && (
            <Button>
              <Link to="/analysis" search={getSearchParams(selectedPlace)}>
                Sparmöglichkeiten aufzeigen
              </Link>
            </Button>
          )}

          {!selectedPlace && (
            <Button disabled> Sparmöglichkeiten aufzeigen</Button>
          )}
        </div>
      </div>
    )
  },
})
