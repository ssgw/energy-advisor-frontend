import { Route } from '@tanstack/react-router'
import { rootRoute } from './root'
import { AddressAutocomplete } from '@/components/address-autocomplete'
import { z } from 'zod'
import { EnergyStat, QuestionItem } from '@/components/energy-stat'
import { PropertyEnergyDataService, PropertyEnergyData } from '@/api'
import { Header } from '@/components/header'
import { GeneralStat, GeneralStatProps } from '@/components/general-stat'
import { EnergyStatProps } from '@/components/energy-stat'
import {
  House,
  Calendar,
  ArrowsOut,
  Thermometer,
  Info,
} from '@phosphor-icons/react'
import { useState } from 'react'
import { CarbonWeightProgressBar } from '@/components/progress-bar'
import { HeatingSource } from '@/lib/types'

const initialOptions: EnergyStatProps[] = [
  {
    label: 'Ölheizung',
    disabled: false,
    checked: false,
    value: 20,
  },
  {
    label: 'Gas',
    disabled: false,
    checked: true,
    value: 35,
  },
  {
    label: 'Fernwärme',
    disabled: true,
    checked: false,
    value: 70,
  },
  {
    label: 'Wärmepumpe',
    disabled: false,
    checked: false,
    value: 70,
  },
]

const analysisSchema = z.object({
  street: z.string(),
  zip: z.string(),
  city: z.string(),
})

export const analysisRoute = new Route({
  getParentRoute: () => rootRoute,
  validateSearch: (search) => analysisSchema.parse(search),
  path: '/analysis',
  loader: async ({ search }) =>
    PropertyEnergyDataService.getApiVPropertyEnergyData(
      '1',
      search.street,
      search.zip,
      search.city
    ),
  component: ({ useSearch, useLoader }) => {
    const [options, setOptions] = useState<EnergyStatProps[]>(initialOptions)
    const [value, setValue] = useState(35)
    const { street, city, zip } = useSearch()
    const formattedAddress = `${street}, ${zip} ${city}`
    const data = useLoader()

    const generalStats: GeneralStatProps[] = [
      {
        prop: 'Klasse',
        value: data.buildingClassification ?? 'U',
        Icon: House,
      },
      {
        prop: 'Wärmequelle',
        value: HeatingSource[data.heatingSystem!.energyHeatSource!],
        Icon: Thermometer,
      },
      {
        prop: 'Energiebezugsfläche',
        value:
          data.energyReferenceSurface === -1
            ? '211'
            : data.energyReferenceSurface?.toString() ?? '211',
        Icon: ArrowsOut,
      },
      {
        prop: 'Gasanschluss',
        value: data.extended.raw.hasGas ? 'Ja' : 'Nein',
        Icon: Info,
      },
    ]
      .concat(
        data.constructionYear && data.constructionYear !== -1
          ? [
              {
                prop: 'Baujahr',
                value: data.constructionYear.toString(),
                Icon: Calendar,
              },
            ]
          : []
      )
      .concat(
        data.boiler !== -1
          ? [
              {
                prop: 'Warmwasserquelle Refnr.',
                value:
                  data.boiler.energyHeatSource?.toString() ?? 'keine Angabe',
                Icon: Info,
              },
            ]
          : []
      )
      .concat(
        data.heatingSystem !== -1
          ? [
              {
                prop: 'Heizungsquelle Refnr.',
                value:
                  data.boiler.energyHeatSource?.toString() ?? 'keine Angabe',
                Icon: Info,
              },
            ]
          : []
      )
      .concat(
        data.extended.raw.hestia_beschreibung !== -1
          ? [
              {
                prop: 'Hestia Beschreibung',
                value:
                  data.extended.raw.hestia_beschreibung?.toString() ??
                  'keine Angabe',
                Icon: Info,
              },
            ]
          : []
      )
      .concat(
        data.extended.raw.gasversorgung !== -1
          ? [
              {
                prop: 'Gaserschliessung',
                value:
                  data.extended.raw.gasversorgung?.toString() ?? 'keine Angabe',
                Icon: Info,
              },
            ]
          : []
      )
      .concat(
        data.extended.raw.waermeversorgung !== -1
          ? [
              {
                prop: 'Wärmeversorgung',
                value:
                  data.extended.raw.waermeversorgung?.toString() ??
                  'keine Angabe',
                Icon: Info,
              },
            ]
          : []
      )

    function setSelectedOption(selectedLabel: string) {
      const newOptions = initialOptions.map((option) => ({
        ...option,
        checked: (option.checked = option.label === selectedLabel),
      }))
      setOptions(newOptions)

      const newValue = initialOptions.filter(
        (option) => option.label === selectedLabel
      )[0]
      setValue(newValue.value)
    }

    return (
      <>
        <Header address={formattedAddress} />
        <div className="sticky top-0 px-4 pt-2 pb-8 w-full ">
          <CarbonWeightProgressBar
            domain={[0, 75]}
            components={[{ step: 'Heizung', value: value }]}
          />
        </div>
        <div className="mx-auto max-w-4xl px-3 grid gap-3">
          <div className="grid grid-cols-4 gap-2">
            {generalStats.map((props, i) => (
              <GeneralStat key={i} {...props} />
            ))}
          </div>

          <QuestionItem
            label="Heizungstyp"
            options={options}
            setSelectedOption={setSelectedOption}
          />
        </div>
      </>
    )
  },
})
