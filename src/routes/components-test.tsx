import { CarbonWeightProgressBar } from '@/components/progress-bar'
import { SwitchButton, SwitchButtonItem } from '@/components/switch-button'
import { Place } from '@/lib/models/place'
import { Route } from '@tanstack/react-router'
import { rootRoute } from './root'
import { QuestionItem } from '@/components/energy-stat'
import { useState } from 'react'

const items: SwitchButtonItem[] = [
  { label: 'Continue' },
  { label: 'Go' },
  { label: 'OK' },
  { label: 'Submit' },
  { label: 'Click me' },
]

const logPlaceData = (place: Place) => {
  console.log({
    address: place.formatted_address,
    lat: place.geometry.location.lat(),
    lng: place.geometry.location.lng(),
  })
}

export const componentsRoute = new Route({
  path: '/components-test',
  component: () => {
    const initialOptions: EnergyStatProps[] = [
      {
        label: 'Heizöl',
        disabled: false,
        checked: false,
      },
      {
        label: 'Gas',
        disabled: false,
        checked: false,
      },
      {
        label: 'Fernwärme',
        disabled: true,
        checked: false,
      },
      {
        label: 'Wärmepumpe',
        disabled: false,
        checked: true,
      },
    ]
    const [options, setOptions] = useState(initialOptions)

    function setSelectedOption(selectedLabel: string) {
      const newOptions = options.map((option) => ({
        ...option,
        checked: (option.checked = option.label === selectedLabel),
      }))
      setOptions(newOptions)
    }

    return (
      <>
        <div className="grid grid-cols-3">
          <div className="col-span-1">Hello</div>
          <div className="col-span-2">
            <SwitchButton items={items} />
          </div>
          {/* <AddressAutocomplete setPlaceDetails={(place) => logPlaceData(place)} /> */}
          <CarbonWeightProgressBar
            domain={[0, 150]}
            components={[
              { step: '1', value: 40 },
              { step: '2', value: 50 },
              { step: '3', value: 25 },
            ]}
          />
          <CarbonWeightProgressBar
            domain={[0, 120]}
            components={[
              { step: '11', value: 40 },
              { step: '12', value: 50 },
              { step: '13', value: 25 },
            ]}
          />
        </div>
        <div>
          <QuestionItem
            options={options}
            label="Hi"
            setSelectedOption={(selectedLabel) => {
              console.log(selectedLabel)
              setSelectedOption(selectedLabel)
            }}
          />
        </div>
      </>
    )
  },
  getParentRoute: () => rootRoute,
})
