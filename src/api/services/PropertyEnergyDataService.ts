/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { PropertyEnergyData } from '../models/PropertyEnergyData';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class PropertyEnergyDataService {

    /**
     * @param version
     * @param street
     * @param zipCode
     * @param city
     * @returns PropertyEnergyData Success
     * @throws ApiError
     */
    public static getApiVPropertyEnergyData(
        version: string,
        street?: string,
        zipCode?: string,
        city?: string,
    ): CancelablePromise<PropertyEnergyData> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/v{version}/property-energy-data',
            path: {
                'version': version,
            },
            query: {
                'street': street,
                'zipCode': zipCode,
                'city': city,
            },
        });
    }

}
