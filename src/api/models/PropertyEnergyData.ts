/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Address } from './Address';
import type { Boiler } from './Boiler';
import type { HeatingSystem } from './HeatingSystem';
import type { Kitchen } from './Kitchen';

export type PropertyEnergyData = {
    id?: number;
    egid?: string | null;
    buildingClassification?: string | null;
    constructionYear?: number;
    buildingSurface?: number;
    buildingVolumes?: string | null;
    energyReferenceSurface?: string | null;
    address?: Address;
    boiler?: Boiler;
    heatingSystem?: HeatingSystem;
    kitchen?: Kitchen;
};

