/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { GeoCoordinates } from './GeoCoordinates';

export type Address = {
    street?: string | null;
    zipCode?: number;
    city?: string | null;
    municipality?: string | null;
    canton?: string | null;
    geoCoordinates?: GeoCoordinates;
};

