/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Boiler = {
    heatProducer?: number;
    energyHeatSource?: number;
    informationSource?: number;
    actualizationDate?: string | null;
};

