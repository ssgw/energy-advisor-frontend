/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { BuildingDistrictHeatingConnection } from './BuildingDistrictHeatingConnection';
import type { HeatingSource } from './HeatingSource';
import type { HeatSupplyStrategy } from './HeatSupplyStrategy';

export type HeatingSystem = {
    heatProducer?: number;
    energyHeatSource?: number;
    informationSource?: number;
    actualizationDate?: string | null;
    districtHeatingConnection?: BuildingDistrictHeatingConnection;
    heatingSource?: HeatingSource;
    heatSupplyStrategy?: HeatSupplyStrategy;
    optimalHeatSupplyAchieved?: boolean;
    isDistrictHeatingRecommended?: boolean;
    isHeatpumpRecommended?: boolean;
    isWoodRecommended?: boolean;
};

