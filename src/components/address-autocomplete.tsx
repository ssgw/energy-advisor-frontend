import { Input } from '@/components/ui/input'
import { useRef, useState } from 'react'
import { Place, PlacePrediction } from '@/lib/models/place'
import usePlacesService from 'react-google-autocomplete/lib/usePlacesAutocompleteService'

export type AddressAutocompleteProps = {
  readonly?: boolean
  address: string | undefined
  placeholder: string
  setPlaceDetails?: (place: Place) => void
}

export function AddressAutocomplete({
  readonly,
  address,
  placeholder,
  setPlaceDetails,
}: AddressAutocompleteProps): React.ReactElement {
  const [isVisible, setIsVisible] = useState(true)
  const [placePredictions, setPlacePredictions] = useState([])
  const inputRef = useRef<HTMLInputElement>(null)

  const getPlacePredictions = async (query) => {
    const res = await fetch(
      `https://api-proxy-energy-data-hackdays.fsn13.cynova.dev/?query=${encodeURIComponent(
        query.input
      )}`
    )
    const data = await res.json()
    console.log(
      data.map((description, i) => ({
        place_id: i,
        description: description.address,
      }))
    )
    setPlacePredictions(
      data.map((description, i) => ({
        place_id: i,
        description: description.address,
      }))
    )
  }

  const setSelectedPlace = (place: PlacePrediction) => {
    if (inputRef.current) {
      inputRef.current.value = place.description
      setIsVisible(false)
      const m = place.description.match(/(.+?), (\d+) (.*)/)
      setPlaceDetails({
        street: m[1],
        zip: m[2],
        city: m[3],
      })
    }
  }

  return (
    <>
      <div className="relative w-full">
        <Input
          disabled={readonly}
          className="w-full"
          placeholder={readonly ? address : placeholder}
          onChange={(evt) => {
            getPlacePredictions({ input: evt.target.value })
            setIsVisible(true)
          }}
          onClick={() => setIsVisible(true)}
          ref={inputRef}
        />

        {placePredictions.length > 0 && isVisible && (
          <div className="absolute top-10 z-30 w-full bg-beige rounded-md p-2 px-4 max-h-60 overflow-scroll flex flex-col gap-1">
            {placePredictions.map((item) => (
              <span
                key={item.place_id}
                className="my-4"
                onClick={() => setSelectedPlace(item as PlacePrediction)}
              >
                {item.description}
              </span>
            ))}
          </div>
        )}
      </div>
    </>
  )
}
