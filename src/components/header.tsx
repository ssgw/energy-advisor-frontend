import { Place } from '@/lib/models/place'
import { AddressAutocomplete } from './address-autocomplete'
import { Link } from '@tanstack/react-router'

export function Header({
  address,
  setPlaceDetails,
}: {
  address?: string | undefined
  setPlaceDetails?: (place: Place) => void
}) {
  return (
    <div className="flex gap-4 pt-6 pb-4 px-4 bg-slate-100 mb-3">
      <Link to="/" className="grid">
        <img src="/logo.png" className="w-24 h-auto object-contain" />
        <p className="text-sm whitespace-nowrap text-neutral-900">
          Energy Planer
        </p>
      </Link>

      <AddressAutocomplete
        readonly={typeof address !== 'undefined'}
        address={address}
        placeholder="Addresse eingeben..."
        setPlaceDetails={setPlaceDetails}
      />
    </div>
  )
}
