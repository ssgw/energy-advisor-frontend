import clsx from 'clsx'
import { useState } from 'react'

export type SwitchButtonItem = {
  label: string
}

export type SwitchButtonProps = {
  items: SwitchButtonItem[]
}

export function SwitchButton({ items }: SwitchButtonProps): React.ReactElement {
  const [selectedItem, setSelectedItem] = useState<
    SwitchButtonItem | undefined
  >(undefined)

  return (
    <div className="grid grid-cols-5 gap-2">
      {items.length > 0 &&
        items.map((item) => (
          <button
            key={item.label}
            className={clsx(
              'px-4 py-2 bg-lightgray',
              selectedItem === item && '!bg-cornflower'
            )}
            onClick={() => setSelectedItem(item)}
          >
            {item.label}
          </button>
        ))}
    </div>
  )
}
