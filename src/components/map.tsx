import { GoogleMap, Marker, useJsApiLoader } from '@react-google-maps/api'
import { memo, useCallback, useEffect, useState } from 'react'
import { Button } from './ui/button'

const center = {
  lat: 47.4245,
  lng: 9.3767,
}

export type MapProps = {
  marker: Marker | undefined
}

function _Map({ marker }: MapProps): React.ReactElement {
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: import.meta.env.VITE_GOOGLE_API_MAPS_KEY,
  })

  const containerStyle = {
    height: 400,
    width: window.innerWidth - 2 * 16,
  }

  const [map, setMap] = useState(null)

  useEffect(() => {
    if (map && marker) {
      console.log(marker)
      new google.maps.Marker({
        ...marker,
        map: map,
      })
    }
  }, [marker])

  const onLoad = useCallback(function callback(map) {
    map.zoom = 10
    setMap(map)
  }, [])

  const onUnmount = useCallback(function callback(map) {
    setMap(null)
  }, [])

  const setCenter = () => {
    if (map) {
      console.log(map)
      new google.maps.Marker({
        position: {
          lat: 47.4245,
          lng: 9.3767,
        },
        map: map,
      })
    }
  }

  return (
    <div className="flex flex-row justify-center items-center">
      {isLoaded && (
        <GoogleMap
          mapContainerStyle={containerStyle}
          center={center}
          zoom={10}
          onLoad={onLoad}
          onUnmount={onUnmount}
        >
          {/* Child components, such as markers, info windows, etc. */}
          <></>
        </GoogleMap>
      )}
      {/* <Button onClick={setCenter}></Button> */}
    </div>
  )
}

export const Map = memo(_Map)
