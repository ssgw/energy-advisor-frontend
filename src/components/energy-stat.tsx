import { cn } from '@/lib/utils'
import { Switch } from './ui/switch'

export type QuestionItemProps = {
  label: string
  options: EnergyStatProps[]
  setSelectedOption: (selectedLabel: string) => void
}

export function QuestionItem({
  label,
  options,
  setSelectedOption,
}: QuestionItemProps) {
  const selectedOptionHandler = (label: string) => {
    console.log(label, options)
    setSelectedOption(label)
  }

  return (
    <>
      <h2>{label}</h2>
      <div className="pt-4 grid gap-2 grid-cols-4">
        {options.map((option, i) => (
          <>
            <EnergyStat
              key={i}
              label={option.label}
              disabled={option.disabled}
              checked={option.checked}
              onSelect={() => selectedOptionHandler(option.label)}
            />
          </>
        ))}
      </div>
    </>
  )
}

export type EnergyStatProps = {
  label: string
  disabled?: boolean
  checked: boolean
  value: number
  onSelect?: (selectedLabel: string) => void
}

export function EnergyStat({
  label,
  disabled,
  checked,
  onSelect = () => {},
}: EnergyStatProps) {
  const selectedOptionHandler = (label: string) => {
    console.log(label)
    onSelect(label)
  }

  return (
    <div
      className="flex gap-2 items-center border border-neutral-200 p-3"
      onClick={() => selectedOptionHandler(label)}
    >
      <Switch checked={checked} disabled={disabled} />
      <p className={cn('text-sm font-bold', { 'text-gray-200': disabled })}>
        {checked}
        {label}
      </p>
    </div>
  )
}
