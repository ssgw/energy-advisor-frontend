import { Card, CardContent, CardHeader, CardTitle } from './ui/card'

export type GeneralStatProps = {
  prop: string
  value: string
  Icon?: React.ComponentType
}

export function GeneralStat({ prop, value, Icon }: GeneralStatProps) {
  return (
    <Card>
      <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
        <CardTitle className="text-sm font-medium">{prop}</CardTitle>
        {Icon && <Icon />}
      </CardHeader>
      <CardContent>
        <div className="text-xl font-bold">{value}</div>
        {/* <p className="text-xs text-muted-foreground">Einfamilienhaus</p> */}
      </CardContent>
    </Card>
  )
}
