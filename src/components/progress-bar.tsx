import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from '@radix-ui/react-tooltip'
import clsx from 'clsx'

export type CarbonWeightProgressBarProps = {
  domain: [number, number]
  components: CarbonWeightComponent[]
}

export type CarbonWeightComponent = {
  step: string
  value: number
}

type SumCarbonWeightComponent = CarbonWeightComponent & { total: number }

// divider length
const DIV_L = 2

export function CarbonWeightProgressBar({
  domain,
  components,
}: CarbonWeightProgressBarProps) {
  const total = components.reduce((acc, b) => acc + b.value, 0)
  const [min, max] = domain
  const bounds = max - min
  const progress = total / bounds

  const accComponents = components
    .reduce<SumCarbonWeightComponent[]>((acc, v, i) => {
      return acc.concat([
        { ...v, total: acc[i - 1] ? acc[i - 1].total + v.value : v.value },
      ])
    }, [])
    .filter((v) => v.value !== 0)

  return (
    <div className="flex flex-col gap-2">
      <div className="w-full relative">
        <div
          className={clsx(
            'h-12 w-full rounded-none bg-gradient-to-r from-lime-500 to-emerald-500'
          )}
          style={{
            clipPath: `inset(0 calc(100% - ${progress * 100}%) 0 0 round 5px)`,
            transition: 'clip-path 0.35s cubic-bezier(.31,.52,.16,.83)',
          }}
        >
          <div className="w-full h-full -top-1 absolute right-0" />
        </div>

        {accComponents.map(({ total, step }) => (
          <>
            <TooltipProvider delayDuration={0}>
              <Tooltip>
                <TooltipTrigger asChild>
                  <div
                    className="absolute top-0 left-0 h-full"
                    style={{
                      left: `${
                        (total / bounds) * 100 -
                        (accComponents[0].total / bounds) * 100
                      }%`,
                      width: `${(total / bounds) * 100}%`,
                    }}
                  ></div>
                </TooltipTrigger>
                <div
                  className="absolute top-0 h-full border-r border-lightgray"
                  key={`divider-${step}`}
                  style={{
                    left: `${(total / bounds) * 100}%`,
                    transition: 'left 0.35s cubic-bezier(.31,.52,.16,.83)',
                  }}
                />
                <TooltipContent>
                  <p className="bg-lightgray p-2 rounded-md">
                    {total} tCO2 / Quelle: {step}
                  </p>
                </TooltipContent>
              </Tooltip>
            </TooltipProvider>
          </>
        ))}
      </div>
    </div>
  )
}
