import { createRoot } from 'react-dom/client'
import { App } from './app'
import './app.css'

const el = document.getElementById('root')!
const root = createRoot(el)
root.render(<App />)
