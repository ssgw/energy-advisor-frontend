import { create } from 'zustand'
import { Place } from '../models/place'

export type PlacesStore = PlacesStoreProps & PlacesStoreSetters

export type PlacesStoreProps = {
  selectedPlace: Place | undefined
}

export type PlacesStoreSetters = {
  setSelectedPlace: (place: Place) => void
}

export const usePlacesStore = create<PlacesStore>((set) => ({
  selectedPlace: undefined,
  setSelectedPlace: (place) => {
    set({ selectedPlace: place })
  },
}))

export type PlacesContext = {
  selectedPlace: Place | undefined
  setSelecedPlace: (place: Place) => void
}
