import { PropertyEnergyData } from '@/api'

export type Property = PropertyEnergyData & {
  extended: {
    raw: {
      id: string
      egid: string
      longitude: number
      latitude: number
      address: string
      umsetzung_von: null
      umsetzung_bis: null
      gstat: number
      gkat: number
      gklas: number
      gbauj: number
      gbaum: null
      gebf: string
      gwaerzh1: number
      genh1: number
      gwaerzh2: null
      genh2: null
      gwaerzw1: number
      genw1: number
      gwaerzw2: number
      genw2: number
      hestia_beschreibung: string
      gasversorgung: string
      waermeversorgung: string
      x: number
      y: number
      Dplz4: null
      Garea: number
      Gdekt: null
      Ggdename: null
      Gvol: null
      Gwaerdath1: null
      Gwaerdatw1: null
      Gwaersceh1: null
      Gwaerscew1: null
      PlzPlz6: null
      StrnameDeinr: null
      Wkche: null
      drill_possibility_state: null
      gastw: number
    }
    buildingClass: string[]
    numFloors: number
  }
}
