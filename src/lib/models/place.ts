export type AddressComponent = {
  long_name: string
  short_name: string
  types: string[]
}

export type GoogleLocation = {
  lat: number
  lng: number
}

export type OpeningHoursPeriod = {
  close: {
    date: string
    day: number
    time: string
    truncated?: boolean
  }
  open: {
    date: string
    day: number
    time: string
    truncated?: boolean
  }
}

export type Place = {
  address_components: AddressComponent[]
  adr_address: string
  business_status: string
  current_opening_hours: {
    open_now: boolean
    periods: OpeningHoursPeriod[]
    weekday_text: string[]
  }
  formatted_address: string
  formatted_phone_number: string
  geometry: {
    location: {
      lat: () => number
      lng: () => number
    }
    viewport: {
      south: number
      west: number
      north: number
      east: number
    }
  }
  icon: string
  icon_background_color: string
  icon_mask_base_uri: string
  international_phone_number: string
  name: string
  opening_hours: {
    open_now: boolean
    periods: OpeningHoursPeriod[]
    weekday_text: string[]
  }
  photos: {
    height: number
    html_attributions: string[]
    width: number
  }[]
  place_id: string
  plus_code: {
    compound_code: string
    global_code: string
  }
  price_level: number
  rating: number
  reference: string
  reviews: {
    author_name: string
    author_url: string
    language: string
    profile_photo_url: string
    rating: number
    relative_time_description: string
    text: string
    time: number
  }[]
  types: string[]
  url: string
  user_ratings_total: number
  utc_offset: number
  vicinity: string
  website: string
  html_attributions: any[]
  utc_offset_minutes: number
}

export type PlacePrediction = {
  description: string
  matched_substrings: {
    length: number
    offset: number
  }[]
  place_id: string
  reference: string
  structured_formatting: {
    main_text: string
    main_text_matched_substrings: {
      length: number
      offset: number
    }[]
    secondary_text: string
  }
  terms: {
    offset: number
    value: string
  }[]
  types: string[]
}
