### STAGE 1: Build ###
# FROM node:20-alpine as builder
# if using M1 or other ARM based processor architecture, use below image
FROM --platform=linux/amd64 node:20-alpine as builder
WORKDIR /
COPY package.json pnpm-lock.yaml ./
RUN npm i npm@9.8.1 pnpm -g
RUN pnpm install && mkdir /app && cp -R ./node_modules ./app
WORKDIR /app
COPY . .
RUN echo building web app
RUN pnpm build 

### STAGE 2: Setup ###
FROM nginx:1.23.1-alpine
EXPOSE 8080
COPY nginx-custom.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /app/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
